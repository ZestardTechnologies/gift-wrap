@extends('header')
@section('content')
<script type="text/javascript">
    ShopifyApp.ready(function (e) {
        ShopifyApp.Bar.initialize({
            title: 'Help',
            buttons: {
            }
        });
    });
</script>
<?php $store_name = session('shop'); ?>
<link rel="stylesheet" href="{{ asset('css/design_style.css') }}" />
<div class="container formcolor formcolor_help" >
    <div class=" row">
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">              
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <img src="" class="imagepreview" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <div class="help_page">            
            <div class="col-md-12 col-sm-12 col-xs-12 need_help">                
                <h2 class="dd-help">Need Help?</h2>
                <p class="col-md-12"><b>To customize any thing within the app or for other work just contact us on below details</b></p>
                <div class="col-md-12 col-sm-12 col-xs-12">
                    <ul class="dd-help-ul">
                        <li><span>Developer: </span><a target="_blank" href="https://www.zestard.com">Zestard Technologies Pvt Ltd</a></li>
                        <li><span>Email: </span><a href="mailto:support@zestard.com">support@zestard.com</a></li>
                        <li><span>Website: </span><a target="_blank" href="https://www.zestard.com">https://www.zestard.com</a></li>
                    </ul>
                </div>
            </div>

            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Configuration Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse1"> 
                                        <strong><span class="">How to configure General Setting?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse1" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <ul class="ul-help">
                                        <li>You can easily Enable / Disable the app in <b>"Enable App?"</b> field.</li>
                                        <li>You can change <b>"Gift Wrap Title"</b> and <b>"Gift Wrap Description"</b>, If you don't add any title or description it will take default value.</li>
                                        <li>You can select page where to display Gift wrap and also select if you want to show message note or not.</li>
                                        <li>Just have to add <b>"Gift Wrap Price"</b> and save.</li>
                                    </ul>                                    
                                </div>
                            </div>
                        </div>
                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse2"> 
                                        <strong><span class="">What will happen if we select Cart / Product page?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse2" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>If you select <b>"Cart Page"</b> then the Gift Wrap will display in cart page.</p>
                                    <p>If you select <b>"Product Page"</b> then Gift Wrap will display in every product page and after checking on gift wrap checkbox, a gift wrap product will get add with specified product name so the merchant can get which product to wrap.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse3"> 
                                        <strong><span class="">Has option to set gift message note?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse3" class="panel-collapse collapse">
                                <div class="panel-body">
                                    <p>Yes, there is a option to put gift message note in the app. If the <b>"Gift Message Note"</b> sets to yes then after selecting giftwrap message note will side down so that customer can write gift message note.</p>
                                </div>
                            </div>
                        </div>

                        <div class="panel panel-default">
                            <div class="panel-heading">
                                <h4 class="panel-title">
                                    <p data-toggle="collapse" data-parent="#accordion" href="#collapse4"> 
                                        <strong><span class="">Where to paste the shortcode?</span>
                                            <span class="fa fa-chevron-down pull-right"></span></strong>  
                                    </p>
                                </h4>
                            </div>
                            <div id="collapse4" class="panel-collapse collapse">
                                <div class="success-copied"></div>
                                <div class="panel-body">
                                    <p><b>Step 1</b></p>
                                    <ul>
                                        <li>Check which page you selected above in "Setting Section". </li>
                                    </ul>

                                    <p><b>Step 2 </b>If Selected page is <b>Cart Page</b> then,</p>
                                    <ul>              
                                        <li>Copy the Shortcode from below and paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode-paste1.png') }}"><b> See Example</b></a></li>
                                        <li>If your theme is section theme then paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode.png') }}"><b> See Example</b></a></li>
                                        <li><b>Note: </b>Please paste the shortcode only once.</li>
                                    </ul>

                                    <p><b>Step 3 </b>If Selected page is <b>Product Page</b> then,</p>
                                    <ul>
                                        <li>Copy the Shortcode from below and paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=templates/product.liquid" target="_blank"><b>product.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product.png') }}"><b> See Example</b></a></li>
                                        <li>If your theme is section theme then paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product-template1.png') }}"><b> See Example</b></a></li>
                                        <li><b>Note: </b>Please paste the shortcode only once.</li>
                                    </ul>
                                    <div class="copystyle_wrapper col-md-5">
                                        <textarea rows="1" class="form-control popup_code" id="product-shortcode" disabled><?php echo "{% include 'giftwrap' %}" ?></textarea>
                                        <btn id="copyproductBtn" name="copyproductBtn" value="Copy Popupcode" class="btn btn-info copycss_button copyMe" data-clipboard-target="#product-shortcode" style="display: block;" onclick="copyToClipboard('#product-shortcode')"><i class="fa fa-check"></i> Copy</btn>
                                    </div>
                                </div>
                            </div>
                        </div>

                    </div>
                </div>
            </div>


            <!--Uninstall Process Div start-->
            <div class="col-md-12 col-sm-12 col-xs-12">
                <h2 class="dd-help">Uninstall Instruction</h2> 
                <div class="col-md-12 col-sm-12 col-xs-12 help_accordians">
                    <div class="panel-group" id="accordion">                        
                        <ul class="ul-help">
                            <li>To Remove the App, go to <a href="https://<?php echo $store_name; ?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
                            <li>Click on the delete icon of Zestard Gift Wrap App.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstall-gift wrap.png') }}"><b> See Example</b></a></li>
                            <li>You can delete the product of gift wrap.</li>
                            <li>If Possible then Remove Shortcode where you have Pasted.</li>
                        </ul>                        
                    </div>
                </div>
            </div>
            <!--Uninstall Process Div end-->
        </div>
        <!--Version updates-->
        <!--        <div class="version_update_section">
                    <div class="col-md-6" style="padding-right: 0;">
                        <div class="feature_box">
                            <h3 class="dd-help">Version Updates <span class="verison_no">2.0</span></h3>
                            <div class="version_block">
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-heart"></i></p>
                                        <strong>22 Jan, 2018</strong>
                                        <a href="#"><b>Update</b></a>
                                    </div>
                                    <div class="col-md-8 version_details">
                                        <strong>Version 2.0</strong>
                                        <ul>
                                            <li>Add Delivery Date & Time information under customer order Email</li>
                                            <li>Auto Select for Next Available Delivery Date</li>
                                            <li>Auto Tag Delivery Details to all the Orders</li>
                                            <li>Manage Cut Off Time for Each Individual Weekday</li>
                                            <li>Limit Number of Order Delivery during the given time slot for any day </li>
                                        </ul>
                                    </div>
                                </div>
                                <div class="col-md-12">
                                    <div class="col-md-3 version_date">
                                        <p><i class="glyphicon glyphicon-globe"></i></p>
                                        <strong>20 Dec, 2017</strong>
                                        <a href="#"><b>Release</b></a>
                                    </div>
                                    <div class="col-md-8 version_details version_details_2">
                                        <strong>Version 1.0</strong>
                                        <ul>
                                            <li>Delivery Date & Time Selection</li>
                                            <li>Same Day & Next Day Delivery</li>
                                            <li>Blocking Specific Days and Dates</li>
                                            <li>Admin Order Manage & Export Based on Delivery Date</li>
                                            <li>Option for Cut Off Time & Delivery Time</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="feature_box">
                            <h3 class="dd-help">Upcoming Features</h3>
                            <div class="feature_block">
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox0" name="exclude_block_date_status">
                                        <label for="checkbox0"></label>
                                    </span> 
                                    <strong>Multiple Cutoff Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox4" name="exclude_block_date_status">
                                        <label for="checkbox4"></label>
                                    </span> 
                                    <strong>Multiple Delivery Time Option</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox5" name="exclude_block_date_status">
                                        <label for="checkbox5"></label>
                                    </span> 
                                    <strong>Auto Tag Delivery Details to all the Orders Within Interval of 1 hour from Order Placed Time</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox1" name="exclude_block_date_status">
                                        <label for="checkbox1"></label>
                                    </span> 
                                    <strong>Auto Select for Next Available Delivery Date</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox2" name="exclude_block_date_status">
                                        <label for="checkbox2"></label>
                                    </span> 
                                    <strong>Order Export in Excel</strong>  
                                </div>
                                <div>   
                                    <span class="checkboxFive">
                                        <input type="checkbox" checked disabled value="1" id="checkbox3" name="exclude_block_date_status">
                                        <label for="checkbox3"></label>
                                    </span> 
                                    <strong>Filtering Orders by Delivery Date</strong>  
                                </div>                                        
                            </div>
                            <div>
                                <p class="feature_text">
                                    New features are always welcome send us on : <a href="mailto:support@zestard.com"><b>support@zestard.com</b></a> 
                                </p>
                            </div>
                        </div>
                    </div>
                </div>-->

    </div>
</div>
<script>
    $(document).ready(function () {
        // Add minus icon for collapse element which is open by default
        $(".collapse.in").each(function () {
            $(this).siblings(".panel-heading").find(".fa").addClass("fa-chevron-up").removeClass("fa-chevron-down");
        });
        // Toggle plus minus icon on show hide of collapse element
        $(".collapse").on('show.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-down").addClass("fa-chevron-up");
        }).on('hide.bs.collapse', function () {
            $(this).parent().find("span.fa").removeClass("fa-chevron-up").addClass("fa-chevron-down");
        });
    });
</script>
<script>
    var el1 = document.getElementById('copyproductBtn');
    var el2 = document.getElementById('copyblogBtn');
    var el3 = document.getElementById('copyrandomproductBtn');
    if (el1) {
        el1.addEventListener("click", function () {
            copyToClipboard(document.getElementById("product-shortcode"));
        });
    }
    if (el2) {
        el2.addEventListener("click", function () {
            copyToClipboard(document.getElementById("blog-shortcode"));
        });
    }
    if (el3) {
        el3.addEventListener("click", function () {
            copyToClipboard(document.getElementById("randomproduct-shortcode"));
        });
    }
    /*document.getElementById("copyproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("product-shortcode"));
     });
     document.getElementById("copyblogBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("blog-shortcode"));
     });
     document.getElementById("copyrandomproductBtn").addEventListener("click", function () {
     copyToClipboard(document.getElementById("randomproduct-shortcode"));
     });*/


    function copyToClipboard(elem) {
        var targetId = "_hiddenCopyText_";
        var isInput = elem.tagName === "INPUT" || elem.tagName === "TEXTAREA";
        var origSelectionStart, origSelectionEnd;
        if (isInput) {
            target = elem;
            origSelectionStart = elem.selectionStart;
            origSelectionEnd = elem.selectionEnd;
        } else {
            target = document.getElementById(targetId);
            if (!target) {
                var target = document.createElement("textarea");
                target.style.position = "absolute";
                target.style.left = "-9999px";
                target.style.top = "0";
                target.id = targetId;
                document.body.appendChild(target);
            }
            target.textContent = elem.textContent;
        }
        var currentFocus = document.activeElement;
        target.focus();
        target.setSelectionRange(0, target.value.length);
        var succeed;
        try {
            succeed = document.execCommand("copy");
        } catch (e) {
            succeed = false;
        }
        if (currentFocus && typeof currentFocus.focus === "function") {
            currentFocus.focus();
        }
        if (isInput) {
            elem.setSelectionRange(origSelectionStart, origSelectionEnd);
        } else {
            target.textContent = "";
        }
        return succeed;
    }
</script>
@endsection
