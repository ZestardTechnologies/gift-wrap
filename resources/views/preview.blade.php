<div id="preview_container">
    <div id="dropdown_preview">
        <form name="dropdownform">
            <div class="price-slider">
                <p class="display_title"></p>
                <p class="display_description"></p>
                <select class="donationamount" name="donation_amount" id="donation_amount"></select>
                <input type="submit" name="donate" value="Donate" class="btn button preview-button" disabled="disabled" />
            </div>          
        </form>
    </div>
    <div id="textbox_preview">
        <form name="textbox_form">
            <div class="price-slider">
                <p class="display_title"></p>
                <p class="display_description"></p>
                <input type="number" name="textbox"/>
                <input type="submit" name="donate" value="Donate" class="btn button preview-button" disabled="disabled" />
            </div>          
        </form>
    </div>
    <div id="pricebar_preview">
        <div class="price-box">
            <form class="form-horizontal form-pricing" role="form" name="pricebar_form">
                <div class="price-slider">
                    <p class="display_title"></p>
                    <p class="display_description"></p>
                    <!-- <span id="displaymin_span"></span>-->
                    <input type="range" id="pricebar_slider" />
                </div>
                <div class="price-form">
                    <label class="amountsize" for="amount">Amount: </label>
                    <input type="hidden" id="amount">
                    <p class="price lead" id="amount-label"></p>
                    <p class="price">.00</p>
                </div>
                <div class="price-slider">
                    <button type="submit" class="btn button preview-button" disabled="disabled">Donate </button>
                </div>
            </form>
        </div>
    </div>
</div>