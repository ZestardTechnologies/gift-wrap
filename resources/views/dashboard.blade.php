@extends('header')
@section('content')
<script type="text/javascript">
  ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
      buttons: { 
        primary: {
          label: 'SAVE',
          callback:  function(event){ save(event) } 
        },       
        secondary: [{
          label: 'HELP',
          href : '{{ url('/help') }}',
          loading: true
        }]
        }    
    });
  });
</script>
<div class="overlay"></div>
<div class="dashboard container">
  <div class="giftwrap-container">
    <div class="col-md-12 left-content-setting">
      <div class="subdiv-content settings">
        <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
            <div class="modal-dialog">
                <div class="modal-content">              
                    <div class="modal-body">
                        <button type="button" class="close" data-dismiss="modal">
                            <span aria-hidden="true">×</span>
                            <span class="sr-only">Close</span>
                        </button>
                        <img src="" class="imagepreview" style="width: 100%;">
                    </div>
                </div>
            </div>
        </div>
        <h2 class="sub-heading">Setting</h2>
            <form action="" method="post" name="giftwrap" id="giftwrap" enctype="multipart/form-data"> 
            {{ csrf_field() }}  
            <div id="add_gift_image">    
            <input id="gift_wrap_product_id" name="gift_wrap_product_id" type="hidden">         
              <div class="col-md-4 form-group">
                  <label for="status">Enable App?</label>
                  <select class="form-control" id="status" name="status">
                    <option value="1">Enabled</option>
                    <option value="0">Disabled</option>
                  </select>
                </div>

                <div class="col-md-4 form-group">
                  <label for="select_page">Select Page</label>
                  <select class="form-control" id="select_page" name="select_page">
                    <option value="1">Cart Page</option>
                    <option value="0">Product Page</option>
                  </select>
                </div>

                <div class="col-md-4 form-group">
                  <label for="gift_message">Gift Message Note</label>
                  <select class="form-control" id="gift_message" name="gift_message">                    
                    <option value="0">No</option>
                    <option value="1">Yes</option>
                  </select>
                </div>

                <div class="col-md-6 form-group form_title">
                  <label for="gift_title">Gift Wrap Title</label>
                  <input id="gift_title" name="gift_title" type="text" class="validate form-control" value="Gift Wrap">
                </div>

                <div class="col-md-6 form-group form_amount">
                  <label for="gift_amount">Gift Wrap Price</label>
                    <a href="#" data-toggle="tooltip" title="Add price for giftwrap"><i class="fa fa-question-circle"></i></a>
                  <input id="gift_amount" name="gift_amount" type="number" class="validate form-control" min="0">
                </div>
                                              
                <div class="col-md-6 form-group form_description">
                  <label for="gift_description">Gift Wrap Description</label>
                  <textarea id="gift_description" name="gift_description" class="form-control">Add a Gift Wrap to your Order.</textarea>
                  <p class="gift-note"><b>Note: </b>This description will show on cart page near "gift wrap checkbox". Put your description with maximum 80 charater.</p>
                </div>
              </div>
            </form>
        </div>
      </div>
      <div class="col-md-12 right-content-preview">      
        <div class="subdiv-content">
          <div class="success-copied"></div>
            <div class="col-md-12 view-shortcode">
            <h2 class="sub-heading">Shortcode</h2>
              <textarea id="ticker-shortcode" rows="1" class="form-control short-code"  readonly="">{% include 'giftwrap' %}</textarea>
              <button type="button" onclick="copyToClipboard('#ticker-shortcode')" class="btn tooltipped tooltipped-s copyMe"
              style="display: block;"><i class="fa fa-check"></i>Copy to clipboard</button>
            </div>
          <div class="col-md-12 shorcode_note cartpage">
            <h2 class="sub-heading">Where to paste Shortcode?</h2>
            <p><b>Step 1</b></p>
            <ul>
              <li>Check which page you selected above in "Setting Section". </li>
            </ul>

            <p><b>Step 2 </b>If Selected page is <b>Cart Page</b> then,</p>
            <ul>              
              <li>Copy the Shortcode from above and paste it in <a href="https://<?php echo $shopdomain->domain ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode-paste1.png') }}"><b> See Example</b></a></li>
              <li>If your theme is section theme then paste it in <a href="https://<?php echo $shopdomain->domain ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode.png') }}"><b> See Example</b></a></li>
              <li><b>Note: </b>Please paste the shortcode only once.</li>
            </ul>

            <p><b>Step 3 </b>If Selected page is <b>Product Page</b> then,</p>
            <ul>
            <li>Copy the Shortcode from above and paste it in <a href="https://<?php echo $shopdomain->domain ?>/admin/themes/current?key=templates/product.liquid" target="_blank"><b>product.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product.png') }}"><b> See Example</b></a></li>
            <li>If your theme is section theme then paste it in <a href="https://<?php echo $shopdomain->domain ?>/admin/themes/current?key=sections/product-template.liquid" target="_blank"><b>product-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/product-template1.png') }}"><b> See Example</b></a></li>
            <li><b>Note: </b>Please paste the shortcode only once.</li>
          </ul>
          </div>         
        </div>
    </div>    
  </div>
</div>

<div class="modal fade" id="new_note">
    <div class="modal-dialog">          
		<div class="modal-content">
			<div class="modal-header">
				<button type="button" class="close" data-dismiss="modal">&times;</button>
				<h4 class="modal-title"><b>Note</b></h4>
			</div>
			<div class="modal-body">
				<p>Dear Customer, As this is a paid app and hundreds of customers are using it, So if you face any issue(s) on your store before uninstalling, Please contact support team (<a href="mailto:support@zestard.com">support@zestard.com</a>) or live chat at bottom right to resolve it ASAP.</p>
			</div>        
			<div class="modal-footer">			
				<div class="datepicker_validate" id="modal_div">
					<div>
						<strong>Show me this again</strong>
							<span class="onoff"><input name="modal_status" type="checkbox" checked id="dont_show_again"/>							
							<label for="dont_show_again"></label></span>
					</div>      
				</div>      
			</div>      
		</div>
	</div>
</div>
<?php //echo count($data);die; ?>
<script>

	$("#dont_show_again").change(function(){
		var checked   = $(this).prop("checked");
		var shop_name = "{{ session('shop') }}";			
		if(!checked)
		{
			$.ajax({
				url:'update-modal-status',
				data:{shop_name:shop_name},
				async:false,					
				success:function(result)
				{
					
				}
			});				
			$('#new_note').modal('toggle');
		}
	});
	if("{{ $new_install }}")
	{
		var new_install = "{{ $new_install }}";	
		if(new_install == "Y")
		{
			$('#new_note').modal('show');
		}
	}		

$(document).ready(function() {

    if(parseInt(<?php echo ($data != '' ? true : false) ?>)) {

    var setdata = <?php echo ($data != '' ? $data : 0) ?>;
    //console.log(setdata);
    var setimage = <?php echo ($imagedata != '' ? $imagedata : 0) ?>;    
    //console.log(setimage);
    if(setdata){
      //console.log("if");
        var giftwrap_image = '<div class="col-md-6 form-group" id="check_imagediv"><label class="giftwrap_image" for="giftwrap_image" style="display:block;">Gift Wrap Image</label>'+
            '<div class="image-div"><div class="col-md-2"><img id="set_gift_image" width="50"></div>'+
            '<div class="col-md-10 chooseimage"><input id="upload_gift_image" name="upload_gift_image" type="file" class="validate form-control"></div></div>'+
            '<p class="gift-note"><b>Note: </b>You can change gift wrap image from here, a product will create with name as gift wrap title in backend. Do not delete the product.</p></div>'

            $('#add_gift_image').append(giftwrap_image);
            //console.log(setdata);
            $('#gift_wrap_product_id').val(setdata.giftwrap_id);
            $('#status option[value='+setdata.status+']').attr("selected", "selected").text();
            $('#select_page option[value='+setdata.select_page  +']').attr("selected", "selected").text();
            $('#gift_message option[value='+setdata.gift_message+']').attr("selected", "selected").text();
            $('#gift_title').val(setdata.gift_title);                    
            $('textarea#gift_description').val(setdata.gift_description);
            $('#gift_amount').val(setdata.gift_amount);
            $("#set_gift_image").attr("src", setimage.src);
                        
            //for display the range or dropdown
            if(setdata.gift_title){
                $(".display_title").text(setdata.gift_title);
            }else{
                $(".display_title").css('display','none');
            }
            if(setdata.gift_description){
                $(".display_description").text('"'+setdata.gift_description+'"');
            }else{
                $(".display_description").css('display','none');
            }           
        }
	}        
});
</script>
@endsection