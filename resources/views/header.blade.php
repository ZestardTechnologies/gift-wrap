@yield('header')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <meta name="description" content="">
    <meta name="author" content="">
    <link rel="icon" href="../../favicon.ico">

    <title>Gift Wrap Zestard</title>

    <!-- Bootstrap core CSS -->
    <link rel="stylesheet" href="{{ asset('css/bootstrap.min.css') }}">

    <!-- toastr CSS -->
    <link rel="stylesheet" type="text/css" href="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/css/toastr.css">
    
    <link href="https://fonts.googleapis.com/css?family=Muli" rel="stylesheet">
    <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/font-awesome/4.6.3/css/font-awesome.min.css"> 

    <!-- magnificent popup CSS -->
    <link rel="stylesheet" href="{{ asset('css/magnific-popup.css') }}">
    
    <link rel="stylesheet" href="{{ asset('css/simple-slider.css') }}">

    <!-- custom CSS -->
    <!-- <link rel="stylesheet" href="{{ asset('css/style.css') }}"> -->
    <link rel="stylesheet" href="{{ asset('css/giftwrap-custom.css') }}">
    
    <script type="text/javascript" src="https://code.jquery.com/jquery-3.2.1.min.js"></script>
    <script src="//cdnjs.cloudflare.com/ajax/libs/toastr.js/latest/js/toastr.min.js"></script>
    
    <script src="{{ asset('js/bootstrap.min.js') }}"></script>
    
    <script src="https://code.jquery.com/ui/1.10.4/jquery-ui.min.js"></script>
    <script src="https://code.highcharts.com/highcharts.js"></script>
    <script src="https://code.highcharts.com/modules/exporting.js"></script>
        
    <!-- shopify Script for fast load -->
    <script src="https://cdn.shopify.com/s/assets/external/app.js"></script>
    <script>
      ShopifyApp.init({
        apiKey: '683afe586e88d50a07a951bb2b89bd4b',
        shopOrigin: '<?php echo "https://".session('shop') ?>'
      });

      ShopifyApp.ready(function() {
          ShopifyApp.Bar.initialize({
            icon: "",
            title: '',
            buttons: {}
          });
        });
    </script>
  </head>
<body>
@yield('content')

<script src="{{ asset('js/jquery.copy-to-clipboard.js') }}"></script>
<script src="{{ asset('js/jquery.magnific-popup.min.js') }}"></script>
<script src="{{ asset('js/simple-slider.js') }}"></script>
<script src="{{ asset('js/javascript.js') }}"></script>

<script>  

function startloader(process) {
    if(process == 1){
    $(".overlay").css({  
        'display' : 'block',
        'background-image' : 'url({{ asset('image/loader1.gif') }})',
        'background-repeat': 'no-repeat',
        'background-attachment': 'fixed',
        'background-position': 'center'
    });  
    } else{
    $(".overlay").css({   
        'display' : 'none',
        'background-image' : 'none',
    });  
    }    
}        

    $('.info_css').magnificPopup({
        type: 'image'
    });
    
    //for save configuration
    $("#giftwrap").submit(function(e) {
        save(e);
    });
    function save(e) {
         var gift_title = $('#gift_title').val();
         var gift_amount = $('#gift_amount').val();
         var gift_description = $('#gift_description').val();
         checkvalid = 0;

        if (gift_title == '' && gift_amount == '' && gift_description == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_title').after('<small style="color:red;">This is required field</small>');
            $('#gift_amount').after('<small style="color:red;">This is required field</small>');             
            $('#gift_description').after('<small style="color:red;">This is required field</small>'); 
            $('.form_amount').css( 'margin-bottom', '15px');
            $('.form_title').css( 'margin-bottom', '15px');
            checkvalid = 0;
            return false;
        }else if(gift_title == '' && gift_amount == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_title').after('<small style="color:red;">This is required field</small>');
            $('#gift_amount').after('<small style="color:red;">This is required field</small>');
            $('.form_amount').css( 'margin-bottom', '15px');
            $('.form_title').css( 'margin-bottom', '15px');
            checkvalid = 0;
            return false;
        }else if(gift_amount == '' && gift_description == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_amount').after('<small style="color:red;">This is required field</small>');             
            $('#gift_description').after('<small style="color:red;">This is required field</small>'); 
            $('.form_amount').css( 'margin-bottom', '-5px');
            checkvalid = 0;
            return false;
        }else if(gift_description == '' && gift_title == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_title').after('<small style="color:red;">This is required field</small>');
            $('#gift_description').after('<small style="color:red;">This is required field</small>'); 
            $('.form_title').css( 'margin-bottom', '-5px');
            checkvalid = 0;
            return false;
        }
        else if(gift_title == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_title').after('<small style="color:red;">This is required field</small>');
            $('.form_title').css( 'margin-bottom', '-5px');
            checkvalid = 0;
            return false;
        }else if(gift_amount == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_amount').after('<small style="color:red;">This is required field</small>');
            $('.form_amount').css( 'margin-bottom', '-5px');
            checkvalid = 0;
            return false;
        } else if(gift_description == ''){
            $('#gift_title').nextAll( "small" ).remove(); 
            $('#gift_amount').nextAll( "small" ).remove();
            $('#gift_description').nextAll( "small" ).remove();
            $('#gift_description').after('<small style="color:red;">This is required field</small>');
            $('.form_amount').css( 'margin-bottom', '15px');
            $('.form_title').css( 'margin-bottom', '15px');
            checkvalid = 0;
            return false;
        }
        else{
            checkvalid = 1;
        } 
        if($("#check_imagediv").length){
                var file = document.getElementById("upload_gift_image").files[0];
//                var fileType = file["type"];
//                var ValidImageTypes = ["image/gif", "image/jpeg", "image/png", "image/jpg", "image/svg"];
//                if ($.inArray(fileType, ValidImageTypes) < 0) {
//                    $('.image-div').after('<small style="color:red;">Image format is not valid</small>');
//                    $('.image-div').focus();
//                    var file = "";
//                    checkvalid = 0;                    
//                }
//                else{
//                    checkvalid = 1;
//                }
            }
            else{
                var file = "";
            }    

            var form_data = new FormData();

            form_data.append('gift_wrap_product_id', $('#gift_wrap_product_id').val());
            form_data.append('status', $('#status').val());
            form_data.append('select_page', $('#select_page').val());
            form_data.append('gift_message', $('#gift_message').val());            
            form_data.append('gift_title', $('#gift_title').val());
            form_data.append('gift_description', $('#gift_description').val());
            form_data.append('gift_amount', $('#gift_amount').val());
            form_data.append('gift_image', $('#set_gift_image').attr('src'));        
            form_data.append('file', file);
            form_data.append('_token', $('[name="_token"]').val());       
     
     if(checkvalid == 1){
        startloader(1);
        //for save data
        $.ajax({
            type: "POST",
            dataType:'json',            
            url: "{{ url('giftwrap_save') }}",
            //data: new FormData(this),
            cache: false,
            contentType: false,
            processData: false,
            crossDomain:true,
            data: form_data,
            success: function (msg) {
                startloader(0);
                toastr.options = {
                  "closeButton": true,
                }
                toastr.success("Updated Successfully");

                location.reload();
            }
        });
     }
      // e.preventDefault(); // avoid to execute the actual submit of the form. 
    }

</script>

<script type="text/javascript">
  function copyToClipboard(element) {
    var $temp = $("<input>");
    $("body").append($temp);
    $temp.val($(element).text()).select();
    document.execCommand("copy");
    //alert('Copied!');
    $temp.remove();
  }      
</script>

<script>
  jQuery(document).ready(function(){       
    jQuery(".copyMe").click(function (){
      var count = jQuery('.show').length;
      if(count == 0){
        jQuery(".show").show();
        jQuery(".success-copied").after('<div class="alert alert-success alert-dismissable show"><a href="#" class="close" data-dismiss="alert" aria-label="close">×</a><strong>Success!</strong> Your shortcode has been copied.</div>');
      }
    });
  });
</script>

<script type="text/javascript">
jQuery(function() {
  jQuery('.screenshot').on('click', function() {
    jQuery('.imagepreview').attr('src', jQuery(this).attr('image-src'));
    jQuery('#imagemodal').modal('show');   
  });     
});
</script>

<script>
  @if(Session::has('notification'))

    var type = "{{ Session::get('notification.alert-type', 'info') }}";
    toastr.options = {
      "closeButton": true,
      "debug": false,
      "newestOnTop": false,
      "progressBar": false,
      "positionClass": "toast-top-right",
      "preventDuplicates": false,
      "onclick": null,
      "showDuration": "300",
      "hideDuration": "1000",
      "timeOut": "5000",
      "extendedTimeOut": "1000",
      "showEasing": "swing",
      "hideEasing": "linear",
      "showMethod": "fadeIn",
      "hideMethod": "fadeOut"
    }
    switch(type){
        case 'info':
            toastr.info("{{ Session::get('notification.message') }}");
            break;
        case 'warning':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;
        case 'success':
            toastr.success("{{ Session::get('notification.message') }}");
            break;
        case 'error':
            toastr.error("{{ Session::get('notification.message') }}");
            break;         
        case 'options':
            toastr.warning("{{ Session::get('notification.message') }}");
            break;

    }
  @endif
</script>

<!--Start of Tawk.to Script-->
<script type="text/javascript">
var Tawk_API=Tawk_API||{}, Tawk_LoadStart=new Date();
(function(){
var s1=document.createElement("script"),s0=document.getElementsByTagName("script")[0];
s1.async=true;
s1.src='https://embed.tawk.to/5a2e20e35d3202175d9b7782/default';
s1.charset='UTF-8';
s1.setAttribute('crossorigin','*');
s0.parentNode.insertBefore(s1,s0);
})();
</script>
<!--End of Tawk.to Script-->

</body>
</html>
