{{ $demo->logo }}

Hi {{ $demo->receiver }},

Thanks for Installing Zestard Application.

We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.

Please don\'t feel hesitate to reach us in case of any queries or questions at support@zestard.com

We also do have live chat support services for quick response and resolution of queries.

Please Note: Support se	rvices are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm

Thanks,
Zestard Support