<html>
<head>
<meta name="viewport" content="width=device-width, initial-scale=1">
<style>
	@import url("https://fonts.googleapis.com/css?family=Open+Sans:300,300i,400,400i,600,600i,700,700i,800,800i");
	@media only screen and (max-width:599px) {
		table {
			width: 100% !important;
		}
	}
	
	@media only screen and (max-width:412px) {
		h2 {
			font-size: 20px;
		}
		p {
			font-size: 13px;
		}
		.easy-donation-icon img {
			width: 120px;
		}
	}
</style>
</head>
<body style="background: #f4f4f4; padding-top: 57px; padding-bottom: 57px;">
	<table class="main" border="0" cellspacing="0" cellpadding="0" width="600px" align="center" style="border: 1px solid #e6e6e6; background:#fff; ">
		<tbody>
			<tr>
				<td style="padding: 30px 30px 10px 30px;" class="review-content">
					<p class="text-align:left;"></p>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px; line-height: 25px; margin-top: 0px;"><b>Hi {{ $demo->receiver }}</b>,</p>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Thanks for Installing Zestard Application {{ $demo->app_name }}</p> 
					<p style="font-family: \'Helvetica\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We appreciate your kin interest for choosing our application and hope that you have a wonderful experience.</p>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">Please don\'t feel hesitate to reach us in case of any queries or questions at <a href="mailto:support@zestard.com" style="text-decoration: none;color: #1f98ea;font-weight: 600;">support@zestard.com</a>.</p>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">We also do have live chat support services for quick response and resolution of queries.</p>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 25px;margin-top: 0px;">(Please Note: Support services are available according to the IST Time Zone(i.e GMT 5:30+) as we reside in India. Timings are from 10:00am to 7:00pm)</p>

				</td>
			</tr>

			<tr>
				<td style="padding: 20px 30px 30px 30px;">

					<br>
					<p style="font-family: \'Open Sans\', sans-serif;font-size: 15px;color: dimgrey;margin-bottom: 13px;line-height: 26px; margin-bottom:0px;">Thanks,<br>Zestard Support</p>
				</td>
			</tr>

		</tbody>
	</table>
</body>

