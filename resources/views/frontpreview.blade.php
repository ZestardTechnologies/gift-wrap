<link href="http://ajax.aspnetcdn.com/ajax/jquery.ui/1.8.9/themes/blitzer/jquery-ui.css" rel="stylesheet" type="text/css" />
<script type="text/javascript">
    function startloader(process) {
        if (process == 1) {
            $(".overlay").css({
                'position': 'fixed',
                'display': 'block',
                'background-image': 'url({{ asset('image / loader1.gif') }})',
                'background-repeat': 'no-repeat',
                'background-attachment': 'fixed',
                'background-position': 'center'
            });
        } else {
            $(".overlay").css({
                'position': 'unset',
                'display': 'none',
                'background-image': 'none',
            });
        }
    }
</script>

<script>
    $(document).ready(function () {
        if (shop == "empayar-ailyqairy.myshopify.com")
        {
            //alert();
        }
        var shop_id = '<?php echo $id; ?>';
        var page = '<?php echo $page; ?>';
        var shop_name = Shopify.shop;
        if(shop_name == 'ashlynanne.myshopify.com'){
            $('.show_charge').hide();
        } 
        $.ajax({
            type: "GET",
            url: "https://zestardshop.com/shopifyapp/zestard_gift_wrap/public/front_preview",
            data: {
                'id': shop_id,
                'shop_name': shop_name
            },
            success: function (data) {
                setdata = JSON.parse(data);
                if (setdata) {

                    if (shop_name == "give-personalised-gifts.myshopify.com")
                    {
                        $.ajax({
                            type: "GET",
                            url: "https://zestardshop.com/shopifyapp/zestard_gift_wrap/public/giftwrap_image",
                            data: {
                                'product_id': setdata.giftwrap_id,
                                'variant_id': setdata.variant_id,
                                'shop_name': shop_name
                            },
                            success: function (data) {
                                //console.log(data);
                                //console.log($("#giftwrap_input").after("<img class='giftwrap_image' src='" + data + "' width='40' height='40'></img>"));
                                $(".giftwrap_image").css('cssText', " margin-right: 10px;position:relative;top: 15px;");
                                $(".show_frontend").css("padding-bottom", "20px");
                            }
                        });
                    }
                    //$('#giftwrap_input').after(setdata.gift_description + " (" + setdata.shop_currency + setdata.gift_amount + ")"); 
                    $('.giftwrap_input').after(setdata.gift_description);
                    $('#giftwrap_price').after(setdata.shop_currency + setdata.gift_amount);
                    $('#gift_price').val(setdata.gift_amount);
                    $('#gift_wrap_variant_id').val(setdata.variant_id);
                }
                if (setdata.gift_message == 1) {
                    $('#gift_message_note').val(setdata.gift_message);
                }
                if (setdata.select_page == 1 && page == "cart") {
                    $('.show_frontend').css('display', 'block');
                } else if (setdata.select_page == 0 && page == "product") {
                    $('.show_frontend').css('display', 'block');
                } else {
                    $('.show_frontend').css('display', 'none');
                }
            }
        });
        if (page == "product") {
            $(".giftwrap_input").click(function () {                               
                var gift_message = $('#gift_message_note').val();                
                if (gift_message == 1) {
                    if(shop_name == 'ashlynanne.myshopify.com'){                        
                        $(".show_charge").slideToggle(this.checked);
                    }
                    $(".zt_message_note").slideToggle(this.checked);
                }
            });            
            var button = $('button[type=submit][name=add]');
            if (button.length == 0)
            {
                button = $('input[type=submit][name=add]');
            }
            if (button.length == 0)
            {
                var f = $('form[action="/cart/add"]');
                button = $(f).find(':submit');
            }

            button.click(function (e) {
                var productname = $("[itemprop=name]").text();
                var giftwrap_variant_id = $('#gift_wrap_variant_id').val();
                if ($(".giftwrap_input").is(':checked')) {                    
                    e.stopPropagation();
                    e.preventDefault();

                    $.post('/cart/add.js', {
                        quantity: 1,
                        id: giftwrap_variant_id,
                        properties: {
                            'For': productname,
                            'Note': $("#giftwrap_text_message").val()
                        }
                    }, function (data) {
                        button.closest("form").submit();
                        return false;
                    }, 'json');
                } else if ($(".giftwrap_input").prop('checked', false)) {                    
                    // e.stopPropagation();
                    // e.preventDefault();                    
                    button.closest("form").submit();
                    //jQuery.post('/cart/update.js', 'updates['+giftwrap_variant_id+']=0')
                } else {
                    button.closest("form").submit();
                    //return false;
                }
            });
        } else {
            $(".giftwrap_input").click(function () {
                var gift_message = $('#gift_message_note').val();
                if (gift_message == 1) {                    
                    $(".zt_message_note").slideToggle(this.checked);
                }

                var giftwrap_variant_id = $('#gift_wrap_variant_id').val();
                if ($(".giftwrap_input").is(':checked')) {
                    //$('label.giftwrap').css('background-color','#7796a8 !important');
                    //$('label.giftwrap:after').css('opacity','1');
                    $.post('/cart/add.js', {
                        quantity: 1,
                        id: giftwrap_variant_id,
                    });
                } else if ($(".giftwrap_input").prop('checked', false)) {
                    //$('label.giftwrap').css('background-color','#7796a8 !important');
                    //$('label.giftwrap:after').css('opacity','0');                    
                    $.post('/cart/update.js', 'updates[' + giftwrap_variant_id + ']=0')
                }
            });
        }
    });

</script>
<!-- <div class="overlay" ></div> -->
<?php 
    $shop = "https://".session('shop');
    $charge_label = "";
    $msg_label = "";
    if($shop == 'https://biombo.com.pe'){
        $charge_label = 'Precio por envoltura de regalo';
        $msg_label = 'Mensaje para la tarjeta de regalo';
    } else {
        $charge_label = 'Gift Wrap Charges';
        $msg_label = 'Gift Message Note';
    }
?>
<div class="show_frontend" style="display:none;">
    <div class="show_desc">
        <input type="hidden" name="gift_message_note" id="gift_message_note">
        <input type="hidden" name="gift_wrap_variant_id" id="gift_wrap_variant_id">
        <span class="checkboxFive">
            <input type="checkbox" name="giftwrap_frontend" id="giftwrap_input" class="giftwrap_input">        
            <label for="giftwrap_input" class="giftwrap"></label>
        </span>
    </div>
    <div class="show_charge">        
        <label id="giftwrap_price" for="giftwrap_price"><b>{{ $charge_label }} </b></label>
        <input type="hidden" name="gift_price" id="gift_price">
    </div>
    <div class="zt_message_note" style="display:none;">
        <label id="giftwrap_message" for="giftwrap_message"><b>{{ $msg_label }} </b></label>
        <textarea name="attributes[gift-message-note]" id="giftwrap_text_message" class="form-control" placeholder="Gift Message Note"></textarea>
    </div>
</div>

<style type="text/css">
    /*checkbox design start*/
    /*.checkboxFive label {
        cursor: pointer;
        position: absolute;
        width: 14px;
        height: 14px;
        top: 0;
        left: 0;
        background: transparent;
        border-radius: 2px;
        border: 1px solid #7796a8;
        margin-top: 2px;
    }
    input[type=checkbox]{visibility: hidden;}
    .checkboxFive {
        position: relative;
        width: 24px;
        height: 24px;
        margin-right: 5px;
        font-size: 14px;
        display: inline;
    }
    .checkboxFive label:after {
        /*opacity: 0;*/
    /*  content: '';
      position: absolute;
      width: 8px;
      height: 5px;
      background: transparent;
      top: 2px;
      left: 2px;
      border: 2px solid #fff;
      border-top: none;
      border-right: none;
      -webkit-transform: rotate(-45deg);
      -moz-transform: rotate(-45deg);
      -o-transform: rotate(-45deg);
      -ms-transform: rotate(-45deg);
      transform: rotate(-45deg);
  }*/
    .checkboxFive input[type=checkbox]:checked + label:after{opacity: 1;}
    .checkboxFive input[type=checkbox]:checked + label{background-color:#7796a8;}
    /*checkbox design end*/

    .overlay {
        top: 0;
        bottom: 0;
        left: 0;
        right: 0;
        background: rgba(0, 0, 0, 0.7);
        transition: opacity 500ms;
        z-index: 999;
    }
    .overlay:target {
        visibility: visible;
        opacity: 1;
    }    
    #giftwrap_input {
        margin-right: 5px;
        margin-left: 5px;
        margin-top: 0;
    }
    .zt_message_note{
        margin-top: 10px;
        float: left;
        width: 100%;
    }
    .show_frontend {
        float: left !important;
        width: 100%;
        padding: 8px 12px;
        background-color: #F4F6F8;
        border-radius: 3px;
        border: 1px solid #b1c9d8;
        box-shadow: 0 2px 2px 0 rgba(0,0,0,0.16), 0 0 0 1px rgba(0,0,0,0.08);
        text-align: left;
    }
    .cart-tools .show_frontend{
        float: left !important;
        width: 50%;
    }
    #giftwrap_price {
        display: inline-block;
        padding-right: 10px;
        margin-top: 0;
        margin-bottom: 0;
        font-size: 13px;
    }
    .show_charge {
        float: none;
    }
    .show_desc {
        float: none;
    }
</style>