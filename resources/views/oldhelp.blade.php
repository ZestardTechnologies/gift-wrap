@extends('header')
@section('content')
<script type="text/javascript">
  ShopifyApp.ready(function(e){
    ShopifyApp.Bar.initialize({
    title: 'Help',
      buttons: {
        }    
    });
  });
</script>

<?php
	$store_name = session('shop');	   					
?>
    <div class="container">
        <div class="row">
            <div class="card">
                <div class="modal fade" id="imagemodal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel" aria-hidden="true" style="display: none;">
                    <div class="modal-dialog">
                        <div class="modal-content">              
                            <div class="modal-body">
                                <button type="button" class="close" data-dismiss="modal">
                                    <span aria-hidden="true">×</span>
                                    <span class="sr-only">Close</span>
                                </button>
                                <img src="" class="imagepreview" style="width: 100%;">
                            </div>
                        </div>
                    </div>
                </div>
                <div class="card-content help">
                    <h5>Need Help?</h5>
					<p><b>To customize any thing in the app or for other work just contact us on below details</b></p>
					<ul>
						<li>Developer: <b><a target="_blank" href="https://www.zestardshop.com">Zestard Technologies Pvt Ltd</a></b></li>
						<li>Email: <b><a href="mailto:support@zestard.com">support@zestard.com</a></b></li>
						<li>Website: <b><a target="_blank" href="https://www.zestardshop.com">https://www.zestardshop.com</a></b></li>
					</ul>
					<hr>
					<h5>General Instruction</h5>
					<h6><b>App Configuration:</b></h6>
					<ul class="limit">
						<li>After installation, dashboard page will be display, where store owner can configure the app.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/dashboard-giftwrap.png') }}"><b> See Example</b></a></li>
						<li>In the Setting Section, there are options for Adding/Updating Gift Wrap title, price, description and show gift message box.</li>		
						<li>After Save Gift Wrap product will create, and after that store owner can change the gift wrap product image.</li>
						<li>On clicking "Copy to clipboard" shortcode will be copied, you can paste it in your cart page.(<b>follow below steps</b>).</li>
					</ul>
					<h6><b>Uninstall App:</b></h6>
					<ul class="limit">
						<li>To Remove the App, go to <a href="https://<?php echo $store_name;?>/admin/apps" target="_blank"><b>Apps</b></a>.</li>
						<li>Click on the delete icon of Zestard Gift Wrap App.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/uninstall-gift wrap.png') }}"><b> See Example</b></a></li>
						<li>You can delete the product of gift wrap.</li>
						<li>If Possible then Remove Shortcode where you have Pasted.</li>
					</ul>
					<hr>
					<h5>Follow the Steps:</h5>
					<h6><b>Where to paste shortcode?</b></h6>
					<ul class="limit">
						<li>After copying shortcode, you can paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=templates/cart.liquid" target="_blank"><b>cart.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode-paste.png') }}"><b> See Example</b></a></li>
						<li>If your theme is section theme then paste it in <a href="https://<?php echo $store_name ?>/admin/themes/current?key=sections/cart-template.liquid" target="_blank"><b>cart-template.liquid</b></a>.<a class="screenshot" href="javascript:void(0)" image-src="{{ asset('image/shortcode-paste2.png') }}"><b> See Example</b></a></li>						
					</ul>
					<a class="goback" href="{{ url('dashboard') }}">
						<img src="{!! asset('image/back.png') !!}">Go Back
					</a>
				</div>				
            </div>
        </div>
    </div>
@endsection
<style>
.limit {
	margin-left: 20px;
}
ul.limit li {
	list-style-type: disc !important;	
}
</style>