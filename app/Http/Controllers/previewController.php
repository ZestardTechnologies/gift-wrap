<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App;
use App\ShopModel;
use DB;

class previewController extends Controller
{
    public function index(Request $request) {
        
		//return $request;
        $sh = App::make('ShopifyAPI');
        $id = $request['id'];
        $page = $request['page'];
        //return $page;
        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
		
        $shop = (array)$shopData;
        $shop_id = $shop['id'];
        $statusdata = DB::table('gift_wrap_settings')->select('status')->where('shop_id', $shop_id)->first();
        $status = (array)$statusdata;
        $appstatus = $status['status'];    
           
		//dd($appstatus);
        if($appstatus == 1){
            return view('frontpreview',['id' => $id, 'page'=>$page]);
        }          
    } 
    public function checkgiftwrap(Request $request) {
        
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $id = $request['id'];

        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
        $shop = (array)$shopData;
        $shop_id = $shop['id'];

        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('id' , $shop_id)->first();
        $shop = $shop_find->store_name;
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);
        
        $amount = $request['data'];
        
        //api for update product price
        $product_argument = [
            'product' => [
                 'id' => $shop_find->product_id,
                'variants' => [
                    '0' => [
                        'price' => $amount
                        ]
                    ]
                ]
        ];
        //api call for product update
        $product = $sh->call(['URL' => '/admin/products/'.$shop_find->product_id.'.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
        
        //api call for product
        $product_variant = $sh->call(['URL' => '/admin/products/'.$shop_find->product_id.'.json', 'METHOD' => 'GET']);
        $varient = $product_variant->product->variants['0']->id;
        return $varient;
        
    }
}
