<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Http\Requests;
use App\ShopModel;
use App\GiftWrapSettings;
use App\Symbol;
use App;
use DB;
use File;
use Mail;

class GiftWrapController extends Controller
{
    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');
        $gift_wrap = new GiftWrapSettings;
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $shop_id = $shop_find->id;
        $giftwrap_config = GiftWrapSettings::where('shop_id' , $shop_id)->first();
        return json_encode($giftwrap_config);
    }
    public function store(Request $request) {      
        $input = $request->all();     
        // echo '<pre>';print_r($input['gift_wrap_product_id']);die;   
        // echo '<pre>';print_r($_FILES);die;
                
        $gift_wrap = new GiftWrapSettings;
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $shop_id = $shop_find->id;
                
        $giftwrap_config = GiftWrapSettings::where('shop_id' , $shop_id)->first();

        if(count($giftwrap_config) > 0)
        {
            $giftwrap_config->status = $input['status'];
            $giftwrap_config->select_page = $input['select_page'];
            $giftwrap_config->gift_message = $input['gift_message'];
            $giftwrap_config->gift_title = $input['gift_title'];
            $giftwrap_config->gift_description = $input['gift_description'];
            $giftwrap_config->gift_amount = $input['gift_amount'];         
            $giftwrap_config->save();

            if($_FILES){
                $uploadGiftImage = $_FILES['file']['tmp_name'];                
                //echo $uploadGiftImage;die('die');
            }else{
                $uploadGiftImage = "";
            }
            if($uploadGiftImage){  
                //echo $uploadGiftImage;die('die1');
                // $this->validate($request, [            
                //     'upload_gift_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:845941',
                // ]);
                
                //echo '<pre>';print_r($_FILES);die;
                $extension = pathinfo($_FILES['file']['name'], PATHINFO_EXTENSION);
                $imageName = time().'.'.$extension;
                $path = public_path('image/product/');
                move_uploaded_file($_FILES['file']['tmp_name'], $path.$imageName);
                $img = config('app.url').'public/image/product/'.$imageName; 
                //print_r($img);die();
                $app_settings = DB::table('appsettings')->where('id', 1)->first();
                $select_store = DB::table('usersettings')->where('store_name', $shop_name)->get();
                
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                //dd('test');
                $product_argument = [
                    'product' => [
                        'id' => $input['gift_wrap_product_id'],
                        'title' => $input['gift_title'],
                        'body_html' => $input['gift_description'],
                        'images' =>  array(
                            '0' => array(
                                'src' => $img
                            )
                        ),
                        'variants' => [
                            '0' => [
                                'price' =>$input['gift_amount'],
                                'inventory_policy' => 'continue'
                                ]
                        ],
                        'inventory_policy' => 'continue'
                    ]
                ];                
            } else {
                $app_settings = DB::table('appsettings')->where('id', 1)->first();
                $select_store = DB::table('usersettings')->where('store_name', $shop_name)->get();
                
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $product_argument = [
                    'product' => [
                        'id' => $input['gift_wrap_product_id'],
                        'title' => $input['gift_title'],
                        'body_html' => $input['gift_description'],
                        'variants' => [
                            '0' => [
                                'price' =>$input['gift_amount'],
                                'inventory_policy' => 'continue'
                                ]
                        ],
                        'inventory_policy' => 'continue'
                    ]
                ];                 
            }
            //echo '<pre>';print_r($product_argument);die;
            $product = $sh->call(['URL' => '/admin/products/'.$input['gift_wrap_product_id'].'.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
            //echo '<pre>';print_r($product);die;
            $product_variant = $sh->call(['URL' => '/admin/products/'.$shop_find->product_id.'.json', 'METHOD' => 'GET']);
            $varient = $product_variant->product->variants['0']->id;
			
			$update_usersettings = DB::statement("UPDATE usersettings SET new_install = 'N' WHERE id = $shop_find->id");
			
            $update_variantid = DB::statement("UPDATE gift_wrap_settings SET variant_id = $varient WHERE shop_id = $shop_find->id");
            //echo '<pre>';print_r($product);die;
            
            return $giftwrap_config;
        }
        else{ 
            $gift_wrap->status = $input['status'];
            $gift_wrap->select_page = $input['select_page'];
            $gift_wrap->gift_message = $input['gift_message'];
            $gift_wrap->gift_title = $input['gift_title'];
            $gift_wrap->gift_description = $input['gift_description'];
            $gift_wrap->gift_amount = $input['gift_amount'];          
            $gift_wrap->shop_id = $shop_id;
            $gift_wrap->save();

            //creating the gift wrap product

            $app_settings = DB::table('appsettings')->where('id', 1)->first();
            $select_store = DB::table('usersettings')->where('store_name', $shop_name)->get();
            
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
  
            $product_argument = [
                'product' => [
                    'title' => $input['gift_title'],
                    'body_html' => $input['gift_description'],
                    'vendor' => 'zestard-gift-wrap',
                    'product_type' => 'Gift Wrap',
                    'images' => array(
                        '0' => array(
                            'src' => config('app.url').'public/image/product/giftwrap.jpg'
                        )
                    ),
                    'variants' => [
                            'option1' => 'Gift Wrap',
                            'price' => $input['gift_amount'],
                            'inventory_policy' => 'continue'
                    ],
                    'inventory_policy' => 'continue'
                ]
            ];
            //api call for product
            $product = $sh->call(['URL' => '/admin/products.json', 'METHOD' => 'POST', 'DATA' => $product_argument]);
            $product_id = $product->product->id;
            $variant_id = $product->product->variants[0]->id;
            
            $update_usersettings = DB::statement("UPDATE usersettings SET product_id = $product_id, new_install = 'N' WHERE id = $shop_find->id");
            $update_gift_settings = DB::statement("UPDATE gift_wrap_settings SET giftwrap_id = $product_id, variant_id = $variant_id WHERE shop_id = $shop_find->id");
            
            
            //for update the price of default variant after the product create
            $product_variant_update = [
                'product' => [
                    'id' => $product_id,
                    'title' => $input['gift_title'],
                    'body_html' => $input['gift_description'],
                    'variants' => [
                        '0' => [
                            'id' =>$variant_id, 
                            'price' =>$input['gift_amount'],
                            'inventory_policy' => 'continue'
                            ]
                    ],
                    'inventory_policy' => 'continue'
                ]
            ];
            $product_variant = $sh->call(['URL' => '/admin/products/'.$product_id.'.json', 'METHOD' => 'PUT', 'DATA' => $product_variant_update]);

          return redirect()->route('gift_wrap');
        }        
    }

    public function giftimage(Request $request) {
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $shop_id = $shop_find->id;
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop_name)->get();
        
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

        $giftwrap_id = GiftWrapSettings::where('shop_id', $shop_id)->first();
        
        $product = $sh->call(['URL' => '/admin/products/'.$giftwrap_id->giftwrap_id.'.json?fields=id,images,title', 'METHOD' => 'GET']);
        foreach($product as $data){
            foreach($data->images as $image_data){
                //print_r($image_data->src);die;
            }            
        }
        //print_r($product);die;
        return json_encode($image_data);        
    }
    
    public function front_preview(Request $request) {    
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop_name = session('shop');
        $shop_name = $request['shop_name'];
        //print_r($shop_name);
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);

        $shop_api = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);
        $currency = Symbol::where('currency_code' , $shop_api->shop->currency)->first();
        //print_r($shop_api);exit();     
        $id = $request['id'];           
        $shopData = DB::table('usersettings')->select('id')->where('store_encrypt', $id)->first();
        $shop = (array)$shopData;
        $shop_id = $shop['id'];        
        $gift_wrap = new GiftWrapSettings;        
        $giftwrap_config = GiftWrapSettings::where('shop_id' , $shop_id)->first(); 
        $giftwrap_config->shop_currency = $currency->symbol_html;
        $giftwrap_config->save();       
        return json_encode($giftwrap_config);
    }

    public function giftwrap_image(Request $request) {    
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        //$shop_name = session('shop');
        $shop_name  = $request['shop_name'];
        $product_id = $request['product_id'];
        $variant_id = $request['variant_id'];
        //print_r($shop_name);
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $product = $sh->call(['URL' => "/admin/products/$product_id.json", 'METHOD' => "GET"]);
        $image = $product->product->images[0]->src;
        echo $image;
    }

    public function product_update(Request $request) {
        $productname = $request['product_name'];
        $image = $request['product_image'];        
        
        /* $this->validate($request, [
            'product_name' => '',
            'product_image' => 'image|mimes:jpeg,png,jpg,gif,svg|max:845941',
        ]); */

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);

        if($image){

          $this->validate($request, [            
            'product_image' => 'required|image|mimes:jpeg,png,jpg,gif,svg|max:845941',
          ]);

          $imageName = time().'.'.$image->getClientOriginalExtension();
          $image->move(public_path('image/product'), $imageName);          
          $img = url('image/product/').'/'.$imageName;
          
          //api for update product price
          //dd($productname);
          $product_argument = [
              'product' => [
                  'id' => $shop_find->product_id,
                  'title' => $productname,
                  'images' => [
                          [
                            'src' => $img
                          ]
                    ],
                    'inventory_policy' => 'continue'
                  ]
          ];
        } else {
            $product_argument = [
                'product' => [
                    'id' => $shop_find->product_id,
                    'title' => $productname,
                    'inventory_policy' => 'continue'
                  ]
              ];
        }
        //api call for product update
        $product = $sh->call(['URL' => '/admin/products/'.$shop_find->product_id.'.json', 'METHOD' => 'PUT', 'DATA' => $product_argument]);
        //echo "<pre/>"; print_r($product);exit;
        
        //for deleting image from server
        //File::delete('image/product', $imageName);

        $notification = array(
            'message' => 'Updated Successfully.',
            'alert-type' => 'success'
        );
        return back()->with('notification',$notification);
    }

    public function webhook(Request $request)
    {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop_name = session('shop');
        $shop_find = ShopModel::where('store_name' , $shop_name)->first();
        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop_name, 'ACCESS_TOKEN' => $shop_find->access_token]);
        $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
        $webhookData = [
            'webhook' => [
                'topic' => 'app/uninstalled',
                'address' => config('app.url') . 'uninstall.php',
                'format' => 'json'
            ]
        ];
        $uninstall = $sh->appUninstallHook($shop_find->access_token, $url, $webhookData);        
        dd($sh->call(['URL' => '/admin/script_tags/35777970235.json',
        'METHOD' => 'DELETE']));
        dd($sh->call(['URL' => '/admin/script_tags.json', 
        'METHOD' => 'GET']));
    }
}
