<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use DB;
use App;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use App\ShopModel;
use App\GiftWrapSettings;

class callbackController extends Controller {

    public function index(Request $request) {
        $sh = App::make('ShopifyAPI');

        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($_GET['shop'])) {
            $shop = $_GET['shop'];
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            if (count($select_store) > 0) {
                //session(['shop' => $shop]);
                //return redirect()->route('dashboard');
                //Remove comment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges/' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop ]);
                } else {
                    return redirect()->route('payment_process');
                }
            } else {
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);

                $permission_url = $sh->installURL(['permissions' => array('read_script_tags', 'write_script_tags', 'read_themes', 'write_content', 'write_themes', 'read_products', 'write_products', 'read_orders'), 'redirect' => $app_settings->redirect_url]);
                return redirect($permission_url);
            }
        }
    }

    public function redirect(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        if (!empty($request->input('shop')) && !empty($request->input('code'))) {
            $shop = $request->input('shop'); //shop name

            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();
            if (count($select_store) > 0) {
                /* session(['shop' => $shop]);
                  return redirect()->route('dashboard'); */
                //Remove coment for the Payment method
                $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
                $id = $select_store[0]->charge_id;
                $url = 'admin/recurring_application_charges' . $id . '.json';
                $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
                $charge_id = $select_store[0]->charge_id;
                $charge_status = $select_store[0]->status;
                if (!empty($charge_id) && $charge_id > 0 && $charge_status == "active") {
                    session(['shop' => $shop]);
                    return redirect()->route('dashboard', ['shop' => $shop ]);
                } else {
                    return redirect()->route('payment_process');
                }
            }

            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop]);
            try {
                $verify = $sh->verifyRequest($request->all());
                if ($verify) {
                    $code = $request->input('code');
                    $accessToken = $sh->getAccessToken($code);

                    //$rand=rand(5,15);
                    //$shop_encrypt=crypt($rand,"shop");
                    DB::table('usersettings')->insert(['access_token' => $accessToken, 'store_name' => $shop, 'store_encrypt' => ""]);
                    $shop_find = ShopModel::where('store_name', $shop)->first();
                    $shop_id = $shop_find->id;

                    $gift_wrap_encrypt = crypt($shop_id, "ze");
                    $finaly_encrypt = str_replace(['/', '.'], "Z", $gift_wrap_encrypt);

                    $update_encrypt = ShopModel::where('id', $shop_id)->update(['store_encrypt' => $finaly_encrypt]);

                    $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $shop_find->access_token]);

                    //for creating the uninstall webhook
                    $url = 'https://' . $_GET['shop'] . '/admin/webhooks.json';
                    $webhookData = [
                        'webhook' => [
                            'topic' => 'app/uninstalled',
                            'address' => config('app.url') . 'uninstall.php',
                            'format' => 'json'
                        ]
                    ];
                    $uninstall = $sh->appUninstallHook($accessToken, $url, $webhookData);

                    //api call for get theme info
                    $theme = $sh->call(['URL' => '/admin/themes.json', 'METHOD' => 'GET']);
                    foreach ($theme->themes as $themeData) {
                        if ($themeData->role == 'main') {

                            $snippets_arguments = ['id' => $finaly_encrypt];
                            $theme_id = $themeData->id;
                            $view = (string) View('snippets', $snippets_arguments);

                            //api call for creating snippets
                            $call = $sh->call(['URL' => '/admin/themes/' . $theme_id . '/assets.json', 'METHOD' => 'PUT', 'DATA' => ['asset' => ['key' => 'snippets/giftwrap.liquid', 'value' => $view]]]);
                        }
                    }

                    $script = $sh->call(['URL' => '/admin/script_tags.json', 'METHOD' => 'POST', 'DATA' => ['script_tag' => ['event' => 'onload', 'src' => config('app.url') . 'public/js/giftwrap.js']]]);


                    session(['shop' => $shop]);
                    //return redirect('dashboard');
                    //creating the Recuring charge for app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
						if($shop == "zankar-test.myshopify.com") //apps-testing-store.myshopify.com
						{
							$charge = $sh->call([
							'URL' => $url,
							'METHOD' => 'POST',
							'DATA' => array(
								'recurring_application_charge' => array(
									'name' => 'Zestard Gift Wrap',
									'price' => 0.01,
									'return_url' => url('payment_success'),
									'capped_amount' => 20,
									'terms' => 'Terms & Condition Applied',
									'test' =>true
								)
							)
							], false);
						}
						else
						{
							$charge = $sh->call([
							'URL' => $url,
							'METHOD' => 'POST',
							'DATA' => array(
								'recurring_application_charge' => array(
									'name' => 'Zestard Gift Wrap',
									'price' => 3.99,
									'return_url' => url('payment_success'),
									'capped_amount' => 20,
									'terms' => 'Terms & Condition Applied',
									//'test' =>true
								)
							)
							], false);
						}
                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string)$charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);
                    $shopi_info = $sh->call(['URL' => '/admin/shop.json', 'METHOD' => 'GET']);

                    $headers = 'MIME-Version: 1.0' . "\r\n";
                    $headers .= 'Content-type: text/html; charset=iso-8859-1' . "\r\n";

                    $msg = '<table>
                            <tr>
                                <th>Shop Name</th>
                                <td>' . $shopi_info->shop->name . '</td>
                            </tr>
                            <tr>
                                <th>Email</th>
                                <td>' . $shopi_info->shop->email . '</td>
                            </tr>
                            <tr>
                                <th>Domain</th>
                                <td>' . $shopi_info->shop->domain . '</td>
                            </tr>
                            <tr>
                                <th>Phone</th>
                                <td>' . $shopi_info->shop->phone . '</td>
                            </tr>
                            <tr>
                                <th>Shop Owner</th>
                                <td>' . $shopi_info->shop->shop_owner . '</td>
                            </tr>
                            <tr>
                                <th>Country</th>
                                <td>' . $shopi_info->shop->country_name . '</td>
                            </tr>
                            <tr>
                                <th>Plan</th>
                                <td>' . $shopi_info->shop->plan_name . '</td>
                            </tr>
                          </table>';

                    $store_details = DB::table('development_stores')->where('dev_store_name', $shop)->first();
                       
                    if(count($store_details) <= 0){
                        mail("sejal.zestard@gmail.com", "Gift Wrap App Installed", $msg, $headers);
                        mail("support@zestard.com","Gift Wrap App Installed",$msg,$headers);
                    }
                    
                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } else {
                    // Issue with data
                }
            } catch (Exception $e) {
                echo '<pre>Error: ' . $e->getMessage() . '</pre>';
            }
        }
    }

    public function dashboard(Request $request) {
        $shop = session('shop');
        
		if(empty($shop))
		{
			$shop = $_GET['shop'];
			//$shop = $request->input('shop');
			session(['shop' => $shop]);			
		}
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        
        $shop_model = new ShopModel;
        $shop_find = ShopModel::where('store_name', $shop)->first();
        
        $shop_id = $shop_find->id;
		$new_install = $shop_find->new_install;
        $gift_data_setting = GiftWrapSettings::where('shop_id', $shop_id)->first();
        $giftdata = json_encode($gift_data_setting);

        if ($gift_data_setting) {
            $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
            if ($gift_data_setting->giftwrap_id != NULL) {

                $product = $sh->call(['URL' => '/admin/products/' . $gift_data_setting->giftwrap_id . '.json?fields=id,images,title', 'METHOD' => 'GET']);
                foreach ($product as $data) {
                    foreach ($data->images as $image_data) {
                        //print_r($image_data->src);die;
                    }
                }
                $imagedata = json_encode($image_data);
            }
            //echo "<pre>"; print_r($data);die();
            return view('dashboard', array('shopdomain' => $shop_find, 'data' => $giftdata, 'imagedata' => $imagedata, 'new_install' => $new_install));
        } else {
            return view('dashboard', array('shopdomain' => $shop_find, 'data' => '', 'imagedata' => '', 'new_install' => $new_install));
        }
    }

    public function payment_method(Request $request) {
        $shop = session('shop');
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        if (count($select_store) > 0) {
            $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);

            $charge_id = $select_store[0]->charge_id;
            $url = 'admin/recurring_application_charges/' . $charge_id . '.json';
            $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET']);
            if (count($charge) > 0) {
                if ($charge->recurring_application_charge->status == "pending") {
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "declined" || $charge->recurring_application_charge->status == "expired") {
                    //creating the new Recuring charge after declined app
                    $url = 'https://' . $shop . '/admin/recurring_application_charges.json';
                    $charge = $sh->call([
                        'URL' => $url,
                        'METHOD' => 'POST',
                        'DATA' => array(
                            'recurring_application_charge' => array(
                                'name' => 'Zestard Gift Wrap',
                                'price' => 3.99,
                                'return_url' => url('payment_success'),
                                'capped_amount' => 20,
                                'terms' => 'Terms & Condition Applied',
                                // 'test' => true
                            )
                        )
                            ], false);

                    $create_charge = DB::table('usersettings')->where('store_name', $shop)->update(['charge_id' => (string)$charge->recurring_application_charge->id, 'api_client_id' => $charge->recurring_application_charge->api_client_id, 'price' => $charge->recurring_application_charge->price, 'status' => $charge->recurring_application_charge->status, 'billing_on' => $charge->recurring_application_charge->billing_on, 'payment_created_at' => $charge->recurring_application_charge->created_at, 'activated_on' => $charge->recurring_application_charge->activated_on, 'trial_ends_on' => $charge->recurring_application_charge->trial_ends_on, 'cancelled_on' => $charge->recurring_application_charge->cancelled_on, 'trial_days' => $charge->recurring_application_charge->trial_days, 'decorated_return_url' => $charge->recurring_application_charge->decorated_return_url, 'confirmation_url' => $charge->recurring_application_charge->confirmation_url, 'domain' => $shop]);

                    //redirecting to the Shopify payment page
                    echo '<script>window.top.location.href="' . $charge->recurring_application_charge->confirmation_url . '"</script>';
                } elseif ($charge->recurring_application_charge->status == "accepted") {

                    $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
                    $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
                    $Activatecharge_array = get_object_vars($Activate_charge);
                    $active_status = $Activatecharge_array['recurring_application_charge']->status;
                    $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
                    return redirect()->route('dashboard', ['shop' => $shop ]);
                }
            }
        }
    }

    public function payment_compelete(Request $request) {
        $app_settings = DB::table('appsettings')->where('id', 1)->first();
        $shop = session('shop');
        $select_store = DB::table('usersettings')->where('store_name', $shop)->get();

        $sh = App::make('ShopifyAPI', ['API_KEY' => $app_settings->api_key, 'API_SECRET' => $app_settings->shared_secret, 'SHOP_DOMAIN' => $shop, 'ACCESS_TOKEN' => $select_store[0]->access_token]);
        $charge_id = $_GET['charge_id'];
        $url = 'admin/recurring_application_charges/#{' . $charge_id . '}.json';
        $charge = $sh->call(['URL' => $url, 'METHOD' => 'GET',]);
        $status = $charge->recurring_application_charges[0]->status;

        $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $status]);

        if ($status == "accepted") {
            $active_url = '/admin/recurring_application_charges/' . $charge_id . '/activate.json';
            $Activate_charge = $sh->call(['URL' => $active_url, 'METHOD' => 'POST', 'HEADERS' => array('Content-Length: 0')]);
            $Activatecharge_array = get_object_vars($Activate_charge);
            $active_status = $Activatecharge_array['recurring_application_charge']->status;
            $update_charge_status = DB::table('usersettings')->where('store_name', $shop)->where('charge_id', $charge_id)->update(['status' => $active_status]);
            return redirect()->route('dashboard', ['shop' => $shop ]);
        } elseif ($status == "declined") {
            return redirect()->route('decline');
            //echo '<script>window.top.location.href="https://'.$shop.'/admin/apps"</script>';
        }
    }

    public function declined(Request $request) {
        $shop = session('shop');
        echo '<script>window.top.location.href="https://' . $shop . '/admin/apps"</script>';
    }
	
	public function update_modal_status(Request $request)
	{
		$shop = $request->input('shop_name');		
		$usersettings = DB::table('usersettings')->where('store_name', $shop)->update(['new_install' => 'N']);	
	}

}
