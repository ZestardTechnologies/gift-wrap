/**
 * @license Copyright (c) 2003-2016, CKSource - Frederico Knabben. All rights reserved.
 * For licensing, see LICENSE.md or http://ckeditor.com/license
 */

CKEDITOR.editorConfig = function( config ) {
	// Define changes to default configuration here. For example:
	// config.language = 'fr';
	// config.uiColor = '#AADC6E';
        config.allowedContent=true;
        config.extraAllowedContent = 'style';
        config.removeButtons = 'Save,NewPage,CreateDiv,Print,Templates,Form,Checkbox,Radio,TextField,Textarea,Select,Button,ImageButton,HiddenField,Image,Flash,ShowBlocks,Iframe,RemoveFormat,NumberedList,BulletedList,Blockquote,Maximize,About,BidiLtr,BidiRtl,Language,Anchor,HorizontalRule,Smiley,SpecialChar,PageBreak,Outdent,Indent';
};

