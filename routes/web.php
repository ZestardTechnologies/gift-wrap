<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

/*Route::get('callback', function () {
    dd('test');
});*/

Route::get('dashboard', 'callbackController@dashboard')->name('dashboard');

Route::get('callback', 'callbackController@index')->name('callback');

Route::get('redirect', 'callbackController@redirect')->name('redirect');

Route::any('change_currency', 'callbackController@Currency')->name('change_currency');

Route::get('gift_wrap', 'GiftWrapController@index')->name('gift_wrap');

Route::get('gift_wrap_image', 'GiftWrapController@giftimage')->name('gift_wrap_image');

Route::any('webhook', 'GiftWrapController@webhook')->name('webhook');

Route::post('giftwrap_save', 'GiftWrapController@store')->name('giftwrap_save');

Route::post('product/update', 'GiftWrapController@product_update')->name('product/update');

Route::get('uninstall', 'callbackController@uninstall')->name('uninstall');

Route::get('payment_process', 'callbackController@payment_method')->name('payment_process');

Route::get('payment_success', 'callbackController@payment_compelete')->name('payment_success');

Route::get('preview', 'previewController@index')->middleware('cors')->name('preview');

Route::get('front_preview', 'GiftWrapController@front_preview')->middleware('cors')->name('front_preview');

Route::get('giftwrap_image', 'GiftWrapController@giftwrap_image')->middleware('cors')->name('giftwrap_image');

Route::post('frontend/add_giftwarp', 'previewController@checkgiftwrap')->middleware('cors')->name('frontend/add_giftwarp');

Route::get('declined', 'callbackController@declined')->name('declined');

Route::any('update-modal-status', 'callbackController@update_modal_status')->name('update-modal-status'); 

Route::get('help', function () {
    return view('help');
})->name('help');

Route::get('decline', function () {
    return view('decline');
})->name('decline');

Route::get('mail/send', 'MailController@send');
